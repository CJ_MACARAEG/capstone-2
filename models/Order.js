const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({
    userId: {
        type: String, //mongoose.Schema.Types.ObjectId
        // ref: 'User',
        required: [true, 'UserID is Required.']
    }, 
    products: [
        {
            productId: {
                type: String, //mongoose.Schema.Types.ObjectId
                // ref: 'Product',
                required: [true, 'The Product ID is required']
            },
            quantity: {
                type: Number,
                // required: [true, 'The Quantity is required.']
            }
        }
    ],

    totalAmount: {
        type: Number,
        // required: [true, 'The Total Amount is required.']
    },
    purchaseOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Order', order_schema)