const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration Controller
module.exports.register = (data) => {
  let encypted_password = bcrypt.hashSync(data.password, 10);

  let new_user = new User({
    email: data.email,
    password: encypted_password,
    isAdmin: false,
  });

  return new_user.save().then((created_user, error) => {
    if (error) {
      return false;
    }

    return created_user;
  });
};

// User login Controller
module.exports.login = (data) => {
  return User.findOne({ email: data.email }).then((result) => {
    if (result == null) {
      return {
        message: "User doesn't exist!",
      };
    }

    const is_password_correct = bcrypt.compareSync(
      data.password,
      result.password
    );

    if (is_password_correct) {
      return {
        accessToken: auth.createAccessToken(result),
      };
    }

    return {
      message: "Password is incorrect!",
    };
  });
};

// User Get Details Controller
// module.exports.getUserDetails = () => {
//   return User.find().then((result) => {
//     return result;
//   });
// };