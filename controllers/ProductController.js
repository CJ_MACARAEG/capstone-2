const Product = require("../models/Product");

module.exports.getAllProducts = () => {
  return Product.find({}).then((result) => {
    return result;
  });
};

module.exports.getAllActive = () => {
  return Product.find({isActive: true,}).then((result) => {
    return result;
  });
};

module.exports.getOneProduct = (id) => {
  return Product.findById(id).then((result) => {
    return result;
  });
};

module.exports.createProduct = (name, description, price) => {
  return Product.create({
    name: name,
    description: description,
    price: price,
  }).then((result) => {
    return {
      message: 'this product has been created'
    }
    //return result;
  });
};

module.exports.updateProduct = (id, name, description, price) => {
  return Product.findByIdAndUpdate(id, {
    name: name,
    description: description,
    price: price,
  }).then((result) => {
    return {
      message: 'the product has been updated'
    }
    // return result;
  });
};

module.exports.archiveProduct = (id, _isActive) => {
  return Product.findByIdAndUpdate(id, {
    isActive: false,
  }).then((result) => {
    return {
      message: 'the product has been archieve'
    }
    // return result;
  });
};

module.exports.unarchiveProduct = (id, _isActive) => {
  return Product.findByIdAndUpdate(id, {
    isActive: true,
  }).then((result) => {
    return {
      message: 'the product has been unarchieve'
    }
    // return result;
  });
};

module.exports.deleteTask = (user_id) => {
  return Product.findByIdAndDelete(user_id).then((removedTask, error) => {
    if(error){
      console.log(error)
        return error
      }

    return{
      message: 'The Product has been deleted'
    }
  })
}


